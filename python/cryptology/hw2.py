def toNums(t):
    t = t.lower()
    result = ""
    for c in t:
        if (ord(c)-97 <= 25 and ord(c)-97 >= 0):
            result += str(ord(c) - 97) + " "
    return result[0:len(result)-1]

def count(t):
    data = toNums(t).split(" ")
    a = [0]*26
    b = [0.0]*26
    for x in data:
        a[int(x)] +=1
    for i in range(26):
        b[i] = a[i]*1.0/len(data)
    print (a)
    print (b)
    return b


def circularShift(b, s):
    c = [0.0]*26
    sum =0
    for i in range(26):
        c[(i+s)%26] = b[i]
    for i in range(26):
        sum += b[i] * c[i]
    return sum

def englishDotProducts(t):
    data = count(t)
    for i in range(26):
        print(circularShift(data,i),i)
        


#print (toNums("abcdefwqer"))
circularShift(count("abcdefqwer"),1)

f = open("text/sherlock.txt", "r")

lines = f.readlines()
sherlock = ""
for l in lines:
    sherlock+=l

englishDotProducts(sherlock)