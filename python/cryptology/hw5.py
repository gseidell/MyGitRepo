#run with python not python3

def encrypt1(k,n,m):
    from Crypto.Cipher import AES
    obj = AES.new(k,AES.MODE_CFB,n)
    encrypted = obj.encrypt(m)
    return repr(encrypted)

def encrypt2(k,n,m):
    from cryptography.hazmat.primitives.ciphers.aead import AESGCM
    aesgcm = AESGCM(k)
    encrypted = aesgcm.encrypt(n,m,None)
    return repr(encrypted)

print encrypt2("This is a key123", "This is an IV456","This is a secret message")
print encrypt2("This is a key123", "This is an IV456","This is another message")