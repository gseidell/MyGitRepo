#run with python not python3
import numpy


def genmatrix(n,p):
    return numpy.random.randint(p,size=(n,n))
def genvec(n,p):
    return numpy.random.randint(p,size=(n))
def generr(n,p):
    return numpy.array([x%10 for x in numpy.random.randint(low=-1,high=2,size=(n))])

def gendata(n,p,N):
    result = []
    count = 0
    for x in range(N):
        A = genmatrix(n,p)
        y = genvec(n,p)
        ey = generr(n,p)
        x = genvec(n,p)
        ex = generr(n,p)
        a = numpy.mod(numpy.mod(numpy.dot(numpy.dot(y,A),x),p)+ numpy.dot(y,ex),p)
        b = numpy.mod(numpy.mod(numpy.dot(numpy.dot(y,A),x),p)+ numpy.dot(x,ey),p)
        agree = "NO"
        ahat = bhat = 0
        if (a in range(p/4) or a in range(3*p/4,p)):
            ahat = 1
        if (b in range(p/4) or b in range(3*p/4,p)):
            bhat =1
        if(ahat == bhat):
            agree = "YES"
            count+=1
        result += [(a,b,ahat,bhat,agree)]
    
    return (result,1.0*count/N)

def gendata2(n,p,N):
    return gendata(n,p,n)[1]*100.0

def gendata3(l):
    print("n\tp\tN\tagree")
    for x in l:
        a = gendata2(x[0],x[1],x[2])
        s = ""
        for i in x:
            s+=""+str(i)+'\t'
        print(s+str(int(a)))

print("a\tb\tahat\tbhat\tagree")

b = (gendata(4,19,1),gendata(10,23,1))
for a in b:
    for x in a[0]:
        s = ""
        for i in x:
            s+=""+str(i)+'\t'
        print(s)


primes = [11, 13, 17, 19, 23]
inputs = []
for i in range(5,11):
    for p in primes:
        inputs.append([i,p,1000])
gendata3(inputs)