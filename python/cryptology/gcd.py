
def extended(a,b):
    x = 0
    y = 1
    u = 1
    v = 0
    while a != 0:
        q = b//a
        r = b%a
        m = x-u*q
        n = y-v*q
        
        b = a
        a = r
        x = u
        y = v
        u = m
        v = n
    gcd = b
    if gcd == 1:
        return max(x,y),"inverse"
    return gcd,"gcd"

def euclid(a,b):

    if b == 0:
        return a        
    return euclid(b,a % b)


for a in range(50,61):
    for b in range(50,61):
        print(a,b,extended(a,b))

print ("101")
for i in range (101):
    print (i,101,extended(i,101))