

def f(s,c):
    return [(c[3]*s[3] + c[2]*s[2] + c[1]*s[1]) % 2, s[3],s[2],s[1]]

def f2(s):
    return [(s[3]*s[2]+s[1]+s[0]) % 2, s[3],s[2],s[1]]

bList = []
cList = []
for w in [0,1]:
    for x in [0,1]:
        for y in [0,1]:
            for z in [0,1]:
                bList.append([w,x,y,z])
                cList.append([w,x,y,z])



print("c-vector\tb-vector\tlength")
for y in cList:
    localMax = (0,0,0)
    c = list(reversed(y))
    for x in bList:
        v = list(reversed(x))
        hist = []
        while (f(v,c) not in hist):
            hist.append(f(v,c))
            v = f(v,c)
        if localMax[0] < len(hist):
            localMax = (len(hist),f(v,c),list(reversed(x)))
    print(c, localMax[2],"\t"+str(localMax[0]))   
    



print("b-vector\tlength")
for x in bList:
    v = list(reversed(x))
    hist = []
    while (f(v,c) not in hist):
        hist.append(f(v,c))
        v = f(v,c)
    print(list(reversed(x)),"\t"+str(len(hist)))
    

