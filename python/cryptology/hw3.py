
#g^n mod p
def power(g,n,p):

    if n == 0:
        return 1
    base2 = binary(n)
    mods = [0]*len(base2)
    mods[0] = g % p
    for i in range(1,len(base2)):
        mods[i] = mods[i-1]**2 % p
    
    prod = 1
    for i in range(0,len(base2)):
        if(base2[i]==1):
            prod*=mods[i]

    return prod % p

def binary(n):
    #returns an array with 1s and 0s representing the number in reverse binary
    result = []
    while(n > 0):
        result.append(n%2)
        n//=2
    return result

print(power(99,99,1010))
print(power(111,111,1010))
print(power(200,999,1111))

def testprime(p):
    car = [561,1105,1729,2465,2821,6601,8911]
    if(p in car):
        return False
    
    for i in range(2,int(p**.5)):
        if(i**p %p != i%p):
            return False

    return True

def testsafeprime(p):
    return  (testprime(p) and testprime((p-1)//2))

i = 1000
primes = []
safeprimes = []
while (len(primes) < 10 or len(safeprimes) < 10):
    if(testprime(i) and len(primes)<10):
        primes.append(i)
    if(testsafeprime(i)):
        safeprimes.append(i)
    i+=1
print (primes)
print(safeprimes)


def testGenerator(p,g):
    a = [0]*(p-1)
    for i in range(0,p-1):
        a[i]=power(g,i,p)

    for i in range(1,p):
        if(i not in a):
            return False
    return True

gens = {}
for x in safeprimes:
    for i in range(1,x):
        if testGenerator(x,i):
            gens[x] = i
            break

#takes a really long time. result is stored in object a below
'''
counts = {}
for x in safeprimes:
    counts[x] = 0
    for i in range(1,x):
        if testGenerator(x,i):
            counts[x] +=1
    print(counts[x])
'''

a = {1367: 682, 1283: 640, 1439: 718, 1187: 592, 1487: 742, 1319: 658, 1307: 652, 1619: 808, 1523: 760, 1019: 508}
for v in a:
    print(a[v]/v)

print(gens)
print(a)
