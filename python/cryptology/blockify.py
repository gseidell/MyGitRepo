def blockify(t, b):
	t = t.lower()
	list = " .?!,;:()-\"\'"
	for x in list:
		t = t.replace(x,"")
	for i in range(b, len(t)+(len(t)//b),b+1):
		t = t[0:i] + " " + t[i: len(t)]
	return t


def shift(t,b, s):
	t = blockify(t,b)
	a = "abcdefghijklmnopqrstuvwxyz0123456789"
	for i in range(len(t)):
		if(t[i] != ' '):
			c = a[(a.index(t[i]) + s)%36]
			t = t[0:i] + c + t[i+1:len(t)]
	return t

print (blockify("Glenn Seidell is taking STAT 440 and loving it! The teacher is Natalia Tchetcherina",5))
print (shift("Glenn Seidell is taking STAT 440 and loving it! The teacher is Natalia Tchetcherina",5,13))

print(shift("In Congress, July 4, 1776. The unanimous Declaration of the thirteen united States of America, When in the Course of human events, it becomes necessary for one people to dissolve the political bands which have connected them with another, and to assume among the powers of the earth, the separate and equal station to which the Laws of Nature and of Nature's God entitle them, a decent respect to the opinions of mankind requires that they should declare the causes which impel them to the separation.",5,13))

