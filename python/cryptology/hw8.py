#run with python not python3

import numpy

def genmatrix(n,p):
    return numpy.random.randint(p,size=(n,n))

def generr(n,p):
    return numpy.random.choice([0,1,p-1],n,True,[(n-4.0)/n,2.0/n,2.0/n])

def gendata(n,p,N):
    result = []
    count = 0
    for x in range(N):
        A = genmatrix(n,p)
        y = generr(n,p)
        ey = generr(n,p)
        x = generr(n,p)
        ex = generr(n,p)
        a = numpy.mod(numpy.mod(numpy.dot(numpy.dot(y,A),x),p)+ numpy.dot(y,ex),p)
        b = numpy.mod(numpy.mod(numpy.dot(numpy.dot(y,A),x),p)+ numpy.dot(x,ey),p)
        agree = "NO"
        ahat = bhat = 0
        if (a in range(p/4) or a in range(3*p/4,p)):
            ahat = 1
        if (b in range(p/4) or b in range(3*p/4,p)):
            bhat =1
        if(ahat == bhat):
            agree = "YES"
            count+=1
        result += [(a,b,ahat,bhat,agree)]
        
    return (result,1.0*count/N)

def gendata2(n,p,N):
    r = gendata(n,p,N)
    p = 0.0
    p1 = 0.0
    p0 = 0.0

    for x in r[0]:
        if x[4] == "YES":
            p+=1
            if x[3] == 1:
                p1+=1
            else:
                p0+=1
    
    return (p/N*100, p0/p*100, p1/p*100)

def gendata3(l):

    result = ""
    result += "n\tp\tN\tpragree\tpragree0\tpragree1\n"

    for x in l:
        r = gendata2(x[0],x[1],x[2])
        result += "%d\t%d\t%d\t%d\t%d\t%d\t\n" % (x[0],x[1],x[2],r[0],r[1],r[2])
    
    return(result)

inputs = []
for p in [7,11,31,101]:
    for n in range(5,101):
        if n %5 == 0:
            inputs.append([n,p,1000])

#inputs = [[5,17,5],[6,19,5],[7,23,10]]
d = gendata3(inputs)

out = open("./text/output.txt","w")
out.write(d)