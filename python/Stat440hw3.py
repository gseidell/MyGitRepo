
def probSample(probs):
    result = []
    for i in range(0,len(probs)):
        for j in range(0,len(probs)):
            result.append((i+1,j+1,(probs[i]*probs[j])))
    return result

def sample(y):
    for i in range(0,len)(y):
        for j in range(0,len(y)):
            print (i+1,j+1,(y[i],y[j]))
        
def tp(probs,y):
    result = []
    for i in range(0,len(y)):
        for j in range(0,len(y)):
            t = ((y[i]/probs[i])+(y[j]/probs[j]))/2.0
            result.append((i+1,j+1,t))
            #print (i+1,j+1,t)
    return result

def piI(probs):
    pi = [0.0]*len(probs)
    for i in range(0,len(probs)):
        for j in range(i,len(probs)):
            if (i!=j):
                pi[i] += probs[i]*probs[j]*2
                pi[j] += probs[i]*probs[j]*2
    for i in range(0,len(probs)):
        pi[i]+=probs[i]**2
    return pi

def tpi(probs, y):
    result = []
    pi = piI(probs)
    for i in range(0,len(y)):
        for j in range(0,len(y)):
            if(i==j):
                result.append((i+1,j+1,y[i]/pi[i]))
            else:
                result.append((i+1,j+1, y[i]/pi[i] + y[j]/pi[j]))
    return result

def tg(probs,y):
    result = []

    pi = piI(probs)
    for i in range(0,len(y)):
        for j in range(0,len(y)):
            if(i==j):
                result.append((i+1,j+1, len(y) * y[i]))
            else:
                num = y[i]/pi[i] + y[j]/pi[j]
                dem = 1.0/pi[i] + 1.0/pi[j]
                result.append((i+1,j+1, len(y)* num/dem))
    return result

def expectedValue(estimate,sampleProb):
    sum = 0
    for i in range(0,len(estimate)):
        sum += (estimate[i][2] * sampleProb[i][2])
    return sum

def nybar(y):
    result = []
    for i in range(0,len(y)):
        for j in range(i+1,len(y)):
            result.append( (i+1,j+1, y[i] + y[j]))
    sum = 0
    for x in result:
        sum += x[2]/6.0
    print ("E(x)^2",sum**2)

    sum = 0
    for x in result:
        sum += x[2]**2/6.0
    print ("E(x^2)",sum)


def ratio(y,x):
    result = []
    for i in range(0,len(y)):
        for j in range(i+1,len(y)):    
            xbar = (x[i]+x[j])/2.0
            ybar = (y[i]+y[j])/2.0
            ratio = ybar/xbar
            result.append((i+1,j+1,sum(x,0) * ratio))
    s = 0
    for x in result:
        s += x[2]/6.0
    print ("Sum",s)
    mse = 0
    for x in result:
        mse += (x[2]-8)**2/6.0
    print ("MSE",mse)

'''
a = [.3,.2,.5]
b =[11,6,25]
'''
a = [.24,.29,.27,.2]
b = [1,4,2,1]
c = [18,22,20,15]

t1 = tp(a,b)
t2 = tpi(a,b)
t3 = tg(a,b)

p = probSample(a)

print("P(s)")
for x in p:
    print (x)

print("tp")
for x in t1:
    print (x)
print("Expected = ",expectedValue(t1,p))
print("tpi")
for x in t1:
    print (x)
print("Expected = ",expectedValue(t2,p))

print("tg")
for x in t1:
    print (x)
print("Expected = ",expectedValue(t3,p))

print("N yBar")
nybar(b)
print("Ratio")
ratio(b,c)
#ratio([200,300,500,400],[4,5,8,5])