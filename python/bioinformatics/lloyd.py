
f = open("text/rosalind_ba8c.txt", "r")
k,m = [int(i) for i in f.readline().split()]


points = [i.split() for i in f.readlines()]
for i in range(len(points)):
    for x in range(len(points[i])):
        points[i][x] = float(points[i][x])
    points[i].append(0)

centers = [[j for j in i]for i in points[:k]]

def redefineCenters():
    data = [0]*k
    for i in range(len(data)):
        data[i] = [0]*(m+1)
    
    for i in range(len(points)):
        for j in range(len(points[i])-1):
            data[points[i][-1]][j] += points[i][j]
        data[points[i][-1]][-1] += 1

    for i in range(len(data)):
        for j in range(len(data[i])-1):
            centers[i][j] = data[i][j]/data[i][-1] if data[-1] != 0 else 0
    

def assignCluster(p):
    dists = [0] * k
    for i in range(len(centers)):
        s = 0
        for j in range(m):
            s+=(centers[i][j]-p[j])**2
        dists[i] = s
    p[-1] = dists.index(min(dists))


prev = [0]

while centers != prev:
    prev = [[j for j in i] for i in centers]
    for p in points:
        assignCluster(p)
    redefineCenters()

centers = [[round(j,3) for j in i[:-1]] for i in centers]


temp = [[str(j) for j in i] for i in centers]
temp2 = [" ".join(i) for i in temp]
result = "\n".join(temp2)
print (result)
