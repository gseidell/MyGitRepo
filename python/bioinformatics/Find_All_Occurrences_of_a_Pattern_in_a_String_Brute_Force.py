
import time

t = time.time()

file = open("./text/rosalind_ba1d.txt", "r")
out = open("./text/Find_All_Occurrences_of_a_Pattern_in_a_String_output.txt", "w")
output = ""

pattern = file.readline().strip()
text = pattern + file.read()

okay = True
answers = []

for i in range(len(text)):
    okay = True
    for j in range(i, i + len(pattern)):
        if j - i < len(pattern) and j < len(text) and text[j] != pattern[j - i]:
            okay = False
            break
    if okay:
        answers.append(i)

print(time.time() - t)

ans = ' '.join(str(i) for i in answers)
#print(ans)