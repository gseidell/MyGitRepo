
'''
  CYCLOPEPTIDESEQUENCING(Spectrum)
        Peptides ← a set containing only the empty peptide
        while Peptides is nonempty
            Peptides ← Expand(Peptides)
            for each peptide Peptide in Peptides
                if Mass(Peptide) = ParentMass(Spectrum)
                    if Cyclospectrum(Peptide) = Spectrum
                        output Peptide
                    remove Peptide from Peptides
                else if Peptide is not consistent with Spectrum
                    remove Peptide from Peptides
'''
import time
masses = [57,71,87,97,99,101,103,113,114,115,128,129,131,137,147,156,163,186]

def expand(peps):
    result = []
    for p in peps:
        for m in masses:
            result.append(p + [m])
    return result

def cyclospectrum(pep):
    prefixes = [0]
    result = [0]
    sum = 0
    for i in pep:
        sum += i
        prefixes.append(sum)
    for i in range(len(pep)):
        for j in range(i+1,len(pep)+1):
            result += [prefixes[j]-prefixes[i]]
            if i>0 and j < len(pep):
                result.append(prefixes[-1] - prefixes[j]+prefixes[i])
    result.sort()
    #print (result)
    return result


def cyclopeptideSequencing(spec):
    peps = [[]]
    result = []
    
    while len(peps) != 0:
        torm = []
        peps = expand(peps)

        for i in range(len(peps)):
            if sum(peps[i]) == spec[-1]:
                temp = cyclospectrum(peps[i])
                if temp == spec:
                    result.append(peps[i])
                torm.append(i)
            elif sum(peps[i]) not in spec:
                torm.append(i)
        for x in reversed(torm):
            del peps[x]
    return result

t = time.time()

#s = "0 71 97 99 103 113 113 114 115 131 137 196 200 202 208 214 226 227 228 240 245 299 311 311 316 327 337 339 340 341 358 408 414 424 429 436 440 442 453 455 471 507 527 537 539 542 551 554 556 566 586 622 638 640 651 653 657 664 669 679 685 735 752 753 754 756 766 777 782 782 794 848 853 865 866 867 879 885 891 893 897 956 962 978 979 980 980 990 994 996 1022 1093".split()
#s = "0 113 128 186 241 299 314 427".split()
#s = "0 113 114 128 128 128 129 129 147 156 156 163 241 243 256 257 257 269 270 275 276 319 319 371 385 385 388 397 399 404 404 432 433 475 499 514 517 527 532 532 544 560 562 588 589 628 645 655 660 661 673 690 702 707 716 718 773 775 784 789 801 818 830 831 836 846 863 902 903 929 931 947 959 959 964 974 977 992 1016 1058 1059 1087 1087 1092 1094 1103 1106 1106 1120 1172 1172 1215 1216 1221 1222 1234 1234 1235 1248 1250 1328 1335 1335 1344 1362 1362 1363 1363 1363 1377 1378 1491".split()
s = "0 71 97 103 113 114 128 137 163 163 163 163 184 199 231 251 260 260 266 276 277 300 302 312 326 347 373 380 394 414 414 415 423 423 444 463 465 475 508 517 536 560 572 577 577 578 578 579 586 607 645 674 675 680 692 699 716 723 735 740 741 770 808 829 836 837 837 838 838 843 855 879 898 907 940 950 952 971 992 992 1000 1001 1001 1021 1035 1042 1068 1089 1103 1113 1115 1138 1139 1149 1155 1155 1164 1184 1216 1231 1252 1252 1252 1252 1278 1287 1301 1302 1312 1318 1344 1415".split()
s = [int (i) for i in s]

output = cyclopeptideSequencing(s)
print (output)
out = open("./text/output1.txt","w")
out.write(' '.join(['-'.join([str(j) for j in i]) for i in output]))
print (time.time()-t)