
def GreedyMotifSearch(Dna, k, t):
  BestMotifs = []
  for i in range(0, t):
        BestMotifs.append(Dna[i][0:k])
  n = len(Dna[0])
  for i in range(n-k+1):
      Motifs = []
      Motifs.append(Dna[0][i:i+k])
      for j in range(1, t):
          P = Profile(Motifs[0:j])
          Motifs.append(ProfileMostProbablePattern(Dna[j], k, P))
      if Score(Motifs) < Score(BestMotifs):
          BestMotifs = Motifs
  return BestMotifs

def Score(Motifs):
  k = len(Motifs[0])
  t = len(Motifs)
  consensus = Consensus(Motifs)
  score = 0 
  for i in range(t):
      for j in range(k):
          if consensus[j] != Motifs[i][j]:
              score += 1
  return score

def Consensus(Motifs):
  k = len(Motifs[0])
  count = Count(Motifs)

  consensus = ""
  for j in range(k):
      m = 0
      frequentSymbol = ""
      for symbol in "ACGT":
          if count[symbol][j] > m:
              m = count[symbol][j]
              frequentSymbol = symbol
      consensus += frequentSymbol
  return consensus

def Count(Motifs):
  count = {}
  k = len(Motifs[0])
  for symbol in "ACGT":
      count[symbol] = []
      for j in range(k):
            count[symbol].append(0)            
             
  t = len(Motifs)
  for i in range(t):
      for j in range(k):
          symbol = Motifs[i][j]
          count[symbol][j] += 1
  return count
            
def Profile(Motifs):
  count = {}
  k = len(Motifs[0])
  for symbol in "ACGT":
      count[symbol] = []
      for j in range(k):
            count[symbol].append(0)

  t = len(Motifs)
  for i in range(t):
      for j in range(k):
          symbol = Motifs[i][j]
          count[symbol][j] += 1/t
  return count
  
def ProfileMostProbablePattern(Text, k, Profile):
    mostProbable = ''
    biggestP = -1
    for i in range(len(Text) - k+1):
        p = Pr(Text[i:i+k], Profile)
        if p > biggestP:
            biggestP = p 
            mostProbable = Text[i:i+k]
    return mostProbable

def Pr(Text, Profile):
    p = 1
    for i in range(len(Text)):
        p = p * Profile[Text[i]][i]
    return p

f = open("text/rosalind_ba2d.txt", "r")
k,t = map(int ,f.readline().split())

s = f.readlines()
strings = []
for x in s:
    strings.append(x.strip())


results = GreedyMotifSearch(strings,k,t)

a = ""
for x in results:
    a+=x + "\n"
out = open("./text/output.txt","w")
out.write(a)






  
  