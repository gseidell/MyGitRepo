import time
t = time.time()

f = open("./text/rosalind_ba1d.txt", "r")
s = f.readline()
s=s[0:len(s)-1]
pattern = s

s = f.readline()
s=s[0:len(s)-1]
text= s


def strCompare(a,b):
    for i in range(0,len(a)):
        if(a[i]!=b[i]):
            return False
    return True


results = ""
for i in range(0,len(text)-len(pattern)+1):
    if(strCompare(text[i:i+len(pattern)], pattern)):
        results+= str(i)+" "

#print (results)

out = open("./text/output1.txt","w")
out.write(results)
print(time.time()-t)
