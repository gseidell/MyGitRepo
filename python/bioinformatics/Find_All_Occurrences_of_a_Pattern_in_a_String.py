import time

t = time.time()


file = open("./text/rosalind_ba1d.txt", "r")
out = open("./text/Find_All_Occurrences_of_a_Pattern_in_a_String_output.txt", "w")
output = ""

pattern = file.readline().strip()
text = pattern + file.read()

#pattern = "ATAT"
#text = "ATATGATATATGCATATACTT"

z = [0] * len(text)

j = 0

#computing z values
for i in range(len(pattern), len(text)):
    if j + z[j] < i:
        counter = 0
        while i + counter < len(text) and counter < len(pattern) and text[i + counter] == pattern[counter]:
            counter += 1
        z[i] = counter
    else:
        k = i - j
        if k + z[k] - 1 < z[j]:
            z[i] = z[k]
        elif k + z[k] - 1 > z[j]:
            z[i] = j + z[j] - i
        else:
            counter = 0
            while i + counter < len(text) and counter < len(pattern) and text[i + counter] == pattern[counter]:
                counter += 1
            z[i] = counter

        if i + z[i] >= j + z[j]:
            j = i
        
    if z[i] == len(pattern) and i != 0:
        output += "%d " % (i - len(pattern))

#print(z)
#print(output)
out.write(output)
print(time.time()- t)