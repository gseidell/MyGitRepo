import time

t = time.time()

f = open("./text/rosalind_kmp.txt", "r")
f.readline()
data = f.readlines()
text = ''.join(data)
text = text.replace("\n","")


#text="CAGCATGGTATCACAGCAGAG"

failure = [0]*len(text)
k = 0
for i in range(2, len(text) + 1):
    while k > 0 and text[k] != text[i-1]:
        k = failure[k-1]
    if text[i-1] == text[k]:
        k +=1
    failure[i-1] = k

fString = [str(i) for i in failure]

out = open("./text/output.txt","w")
output = ' '.join(fString)
out.write(output)
print(failure)
print(time.time()-t)