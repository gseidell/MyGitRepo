
f = open("text/rosalind_ba2c.txt", "r")
s = f.readline()
text = s[0:len(s)-1]

s = f.readline()
k = int(s)

prob = []
s = f.readlines()
i = 0
for a in s:
    prob.append(a.split(" "))
    prob[i] = [float(x) for x in prob[i]]
    i +=1

ref = "A C G T".split()

def calc(s):
    prod = 1
    for j in range(0,k):
        prod *= prob[ref.index(s[j])][j]
    return prod


max = 0.0
max = 0.0



winner = ""
for i in range(0,len(text)-k+1):
    p = calc(text[i:i+k])
    if(p>max):
        max = p
        winner = text[i:i+k]

print(winner)

