import requests
import requests.auth

import json
import re

import os
#os.chdir("/home/glenn/Pictures/")
#print(os.getcwd())
imgurHeader = {"Authorization": "Client-ID 58432e0759aaf62"}


client_auth = requests.auth.HTTPBasicAuth('fywCmA21_1D5Kg', 'm2A2N5a9imSAoaJcnmnLqaDgjj4')
post_data = {"grant_type": "password", "username": "WallpaperSteamer", "password": "thisismypassword"}
redditHeaders = {"User-Agent": "rWallpapersScraper/0.1 by WallpaperSteamer"}
response = requests.post("https://www.reddit.com/api/v1/access_token", auth=client_auth, data=post_data, headers=redditHeaders)

#print(response.json())

token = response.json()["access_token"]

redditHeadersWithToken = {"Authorization": "bearer "+token, "User-Agent": "rWallpapersScraper/0.1 by WallpaperSteamer"}
response = requests.get("https://oauth.reddit.com/r/wallpapers/top/?t=week", headers=redditHeadersWithToken)
a = response.json()

#out = open("./text/test.json","w")
#out.write(json.dumps(a))

path = "/home/glenn/Pictures/WeeklyWallpapers/"

def emptyFolder():
    import shutil
    shutil.rmtree(path)
    os.makedirs(path)

def get_valid_filename(s):
    """
    Return the given string converted to a string that can be used for a clean
    filename. Remove leading and trailing spaces; convert other spaces to
    underscores; and remove anything that is not an alphanumeric, dash,
    underscore, or dot.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    s = str(s).strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)

def downloadPic(name,url,location):
    print(name,url)
    img_data = requests.get(url).content
    if(len(name) == 0):
        name = get_valid_filename(url)
    with open(""+location+name, 'wb') as handler:
        handler.write(img_data)

def getHash(url):
    return re.search(r"\w+$",url).group(0)


def downloadAlbum(name,url):
    galleryHash = getHash(url)
    a = "https://api.imgur.com/3/album/"+galleryHash
    response = requests.get(a, headers=imgurHeader)
    data = response.json()
    count= 0
    name = get_valid_filename(name)
    if(len(name) == 0):
        name = get_valid_filename(url)
    
    #import os
    #os.makedirs(path+name)                                         #needed if you want albmums in seperate folder

    for x in data["data"]["images"]:
        print(str(count),x["link"],path+name+"/")
        fileEnding = x["link"][-4:len(x["link"])]
        #downloadPic(str(count)+fileEnding,x["link"],path+name+"/") #stores contents of album in seperate folder path/name/1, path/name/2 ...
        downloadPic(name+str(count)+fileEnding, x["link"], path)    #stores contents of album in same folder path/name1, path/name2 ...
        count+=1

def getLinkFromHash(hash):
    a = "https://api.imgur.com/3/image/"+hash
    response = requests.get(a, headers=imgurHeader)
    data = response.json()
    return data["data"]["link"]

emptyFolder()

#test comment

for x in  a["data"]["children"]:
    print(x["data"]["title"])
    url = x["data"]["url"]
    
    #try:
    if ".com/a/" in url or ".com/gall" in url:
        name = get_valid_filename(x["data"]["title"])
        downloadAlbum(name,url)
    else:
        fileEnding = url[-4:len(url)]
        if '.' not in fileEnding:
            url = getLinkFromHash(getHash(url))
            fileEnding = ""
        name = get_valid_filename(x["data"]["title"])+fileEnding
        downloadPic(name,url,path)
    #except:
    #    print ("Whoops")
    #    print(x["data"]["title"])
    #    print(x["data"]["url"])
    #    print(x["data"]["permalink"])
    #    print("End Whoops")
