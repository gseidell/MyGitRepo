#!/bin/bash
find /home/glenn/Pictures/downloaded/ -maxdepth 3 -mindepth 1 -type f -printf '%p\n' > /home/glenn/Documents/wallpaperFilePaths.txt
p=$(shuf -n 1 /home/glenn/Documents/wallpaperFilePaths.txt)
p="file://"$p
gsettings set org.gnome.desktop.background picture-uri $p
