from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import os

gauth = GoogleAuth()
# Try to load saved client credentials
gauth.LoadCredentialsFile("mycreds.txt")
if gauth.credentials is None:
    # Authenticate if they're not there
    gauth.LocalWebserverAuth()
elif gauth.access_token_expired:
    # Refresh them if expired
    gauth.Refresh()
else:
    # Initialize the saved creds
    gauth.Authorize()
# Save the current credentials to a file
gauth.SaveCredentialsFile("mycreds.txt")

drive = GoogleDrive(gauth)


#file id of the Weekly folder
fid = '1rk-o76cNkE9oAC4e4LhiH78CK1-wyWuV'

def delete():
    file_list = drive.ListFile({'q': "'1rk-o76cNkE9oAC4e4LhiH78CK1-wyWuV' in parents and trashed=false"}).GetList()
    for file1 in file_list:
        fileToDelete = drive.CreateFile({'id': file1['id']})
        fileToDelete.Trash()
        if file1['title'] == "Weekly":
            print (file1)
        print('title: %s, id: %s' % (file1['title'], file1['id']))

def upload():
    path = "/home/glenn/Pictures/WeeklyWallpapers/"
    print(path)
    files = os.listdir(path)
    print (files)
    for filename in files:
        print("Uploading "+filename)

        file5 = drive.CreateFile({'title':filename,"parents": [{"kind": "drive#fileLink", "id": fid}]})
        file5.SetContentFile(path+filename)
        file5.Upload() # Upload the file.
        print("Uploaded "+filename)


delete()
upload()