print("Guess a num between 1 and 100")
from random import randint
target = randint(1,100)

guess = 0
tries = 0
while(guess != target):
    tries +=1
    guess = int(input())
    if(guess < target):
        print ("higher")
    if(guess > target):
        print("lower")

print("yay", tries)