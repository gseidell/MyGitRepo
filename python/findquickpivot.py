l = [3,2,6,7,9,8]
maxs = [-1]*len(l)
maxs[0] = l[0]

mins = [100]*len(l)
mins[len(mins)-1] = l[len(l)-1]
s = len(l)-1
for i in range(1,len(l)):
    maxs[i] = max(maxs[i-1],l[i])
    mins[s-i] = min(mins[s-i+1],l[s-i])

x = list(filter(lambda a: mins[a]==maxs[a],range(len(mins))))

print (x)
print (maxs)
print (mins)