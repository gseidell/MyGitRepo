# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

    def toString(self):
        if self.next is None:
            return self.val
        return str(self.val) +","+ str(self.next.toString())

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        a= 0
        b=0
        counter =0

        while(l1 is not None):
            a = a+(10**counter)*l1.val
            counter = counter+1
            l1 = l1.next

        counter =0
        while(l2 is not None):
            b = b+(10**counter)*l2.val
            counter = counter+1
            l2 = l2.next
        
        c = a+b
        ans = ListNode(c%10)
        current = ans
        c = c//10
        while (c!=0):
            current.next = ListNode(c%10)
            current = current.next
            c = c//10
        
        return ans


la = ListNode(5)
la.next = ListNode(7)
lb = ListNode(4)
lb.next = ListNode(9)

# print(a.toString(),b.toString())
sol = Solution()
print(sol.addTwoNumbers(la,lb).toString())

