
# Buildings that are cool

---

Name: Glenn Seidell

Date: 4/12/19

Project Topic: Buildings

URL: localhost:3000

---


### 1. Data Format and Storage

Data point fields:
- `Field 1`: Name          `Type: String`
- `Field 2`: Height        `Type: Number`
- `Field 3`: Date          `Type: Number`
- `Field 4`: City          `Type: String`
- `Field 5`: LatLong       `Type: [Number]`

Schema: 
```javascript
{
   name: String,
   height: Number,
   date: Number,
   city: String,
   latLong: [Number]
}
```

### 2. Add New Data

HTML form route: `/add`

POST endpoint route: `/api/addPost`

Example Node.js POST request to endpoint: 
```javascript
var request = require("request");

var options = { 
    method: 'POST',
    url: 'http://localhost:3000/api/addPost",
    headers: { 
        'content-type': 'application/x-www-form-urlencoded' 
    },
    form: { 
      name: 'Empire State Building', 
      height: 1434,
      date: 1931,
      city: "New York"
      latLong: [
                40.748433,
                -73.985656
            ]
    } 
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

### 3. View Data

GET endpoint route: `/api/getAll`

### 4. Search Data

Search Field: `name`

### 5. Navigation Pages

Navigation Filters
1. Alphabetical -> `/Alphabetical`
2. Year Erected -> `/Date`
3. City -> `/City`
4. Height -> `/Height`
5. Oldest -> `/Oldest`
