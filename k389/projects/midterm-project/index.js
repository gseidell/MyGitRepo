var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var exphbs = require('express-handlebars');

var dataUtil = require("./data-util");
var _DATA = dataUtil.loadData().buildings;

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.use('/public', express.static('public'));

/* Add whatever endpoints you need! Remember that your API endpoints must
 * have '/api' prepended to them. Please remember that you need at least 5
 * endpoints for the API, and 5 others.
 */

app.get('/', function (req, res) {
  res.render('home', { data: _DATA });
})

app.get('/add/', function (req, res) {
  res.render('add', {});
})

//<Sorry> I know this defeats the puropse of handlebars but I needed 5 pages
app.get('/Alphabetical/', function (req, res) {
  sorted = _DATA.sort((a, b) => a.name < b.name ? -1 : 1)
  res.render('alph', { data: sorted });
})

app.get('/Date/', function (req, res) {
  sorted = _DATA.sort((a, b) => a.date < b.date ? -1 : 1)
  res.render('date', { data: sorted });
})

app.get('/City/', function (req, res) {
  sorted = _DATA.sort((a, b) => a.city.toLowerCase() < b.city.toLowerCase() ? -1 : 1)
  res.render('city', { data: sorted });
})

app.get('/Height/', function (req, res) {
  sorted = _DATA.sort((a, b) => a.height > b.height ? -1 : 1)
  res.render('tall', { data: sorted });
})

app.get('/Oldest/', function (req, res) {
  sorted = _DATA.sort((a, b) => a.date > b.date ? -1 : 1)
  res.render('olde', { data: sorted });
})
//</Sorry>

app.get('/api/getAll/', function (req, res) {
  res.send(_DATA);
})

app.post('/api/addPost', function (req, res) {
  var body = req.body;

  var latLong = [Number(body.lat), Number(body.long)];
  body.latLong = latLong;
  delete body.lat;
  delete body.long;
  body.height = Number(body.height);
  body.date = Number(body.date);

  // Save new blog post
  _DATA.push(req.body);
  dataUtil.saveData(_DATA);
  res.redirect("/");
});

app.listen(3000, function () {
  console.log('Listening on port 3000!');
});
