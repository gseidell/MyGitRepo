// This is a subset of the states.
// Use this to actually run the game
// (assume this is the full set of states.
// This will make it easier to test.
var states = ["Idaho", "South Dakota", "Hawaii", "Alaska", "Alabama", "New York"];

var time = 5;

// These are all the states. It maps the state name to the number which you'll
// want to use in your API call.
var abvMap = {
    "Alabama": "01",
    "Alaska": "02",
    "Arizona": "04",
    "Arkansas": "05",
    "California": "06",
    "Colorado": "08",
    "Connecticut": "09",
    "Delaware": "10",
    "District Of Columbia": "11",
    "Florida": "12",
    "Georgia": "13",
    "Hawaii": "15",
    "Idaho": "16",
    "Illinois": "17",
    "Indiana": "18",
    "Iowa": "19",
    "Kansas": "20",
    "Kentucky": "21",
    "Louisiana": "22",
    "Maine": "23",
    "Maryland": "24",
    "Massachusetts": "25",
    "Michigan": "26",
    "Minnesota": "27",
    "Mississippi": "28",
    "Missouri": "29",
    "Montana": "30",
    "Nebraska": "31",
    "Nevada": "32",
    "New Hampshire": "33",
    "New Jersey": "34",
    "New Mexico": "35",
    "New York": "36",
    "North Carolina": "37",
    "North Dakota": "38",
    "Ohio": "39",
    "Oklahoma": "40",
    "Oregon": "41",
    "Pennsylvania": "42",
    "Rhode Island": "44",
    "South Carolina": "45",
    "South Dakota": "46",
    "Tennessee": "47",
    "Texas": "48",
    "Utah": "49",
    "Vermont": "50",
    "Virginia": "51",
    "Washington": "53",
    "West Virginia": "54",
    "Wisconsin": "55",
    "Wyoming": "56",
}

function checkIfStateEntered(str) {
    // if (states.includes(str.toLowerCase())) {
    if (states.filter((x) => x.toLowerCase() === str.toLowerCase()).length !== 0) {
        str = states.filter((x) => x.toLowerCase() === str.toLowerCase())[0];
        states = states.filter((x) => x.toLowerCase() !== str.toLowerCase());
        $("#correctList").append("<li>" + str + "</li>");
        $("#inputBox").val("");
        addHoverListeners();
        if (states.length === 0) {
            $("#win").text("Win");
            clearInterval(timer);
            displayRemianing();
        }
    }
}

function pullData(state) {
    state = state.split(" ").map(x => (String(x).substring(0, 1)).toUpperCase() + (String(x).substring(1)).toLowerCase()).join(" ");
    stateCode = abvMap[state];
    $.get("https://api.census.gov/data/2013/language?get=EST,LANLABEL,NAME&for=state:" + stateCode + "&LAN=625",
        function (data, textStatus, jqXHR) {
            $("#display").html("<h3>" + state + "</h3> <p>Spanish Speakers: " + Number(data[1][0]).toLocaleString() + "</p>");
        }
    );
}

function reset() {
    states = [];
    for (k in abvMap) {
        states.push(String(k));
    }
    time = 20;
    $("#clock").children("h1").text(time.toFixed(2));
    $("#win").text("");
    $("#remainingList, #correctList, #display").html("");

}

function displayRemianing() {
    for (state of states) {
        $("#remainingList").append("<li>" + state + "</li>");
    }
    addHoverListeners();
}

function addHoverListeners() {
    $("#remainingList, #correctList").children("li").hover(function () { pullData($(this).text()) })
}

function decrementTimer() {
    time -= .01;
    if (time <= 0) {
        clearInterval(timer);
        $("#inputBox").attr("disabled", "disabled");
        time = 0;
        displayRemianing();
        $("#win").text("Lose " + (51 - states.length) + '/' + 51);
    }
    $("#clock").children("h1").text(time.toFixed(2));

}

reset();
$("#start").on("click", function () {
    reset();
    $("#inputBox").removeAttr("disabled");
    $("#inputBox").focus();
    timer = setInterval(() => decrementTimer(), 10);
});

$("#start").focus();
$("#inputBox").on("change paste keyup", () => { checkIfStateEntered($("#inputBox").val()); });

/*
 * The majority of this project is done in JavaScript.
 *
 * 1. Start the timer when the click button is hit. Also, you must worry about
 *    how it will decrement (hint: setInterval).
 * 2. Check the input text with the group of states that has not already been
 *    entered. Note that this should only work if the game is currently in
 * 3. Realize when the user has entered all of the states, and let him/her know
 *    that he/she has won (also must handle the lose scenario). The timer must
 *    be stopped as well.
 *
 * There may be other tasks that must be completed, and everyone's implementation
 * will be different. Make sure you Google! We urge you to post in Piazza if
 * you are stuck.
 */
