var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var pokeDataUtil = require("./poke-data-util");
var _ = require("underscore");
var app = express();
var PORT = 3000;

// Restore original data into poke.json. 
// Leave this here if you want to restore the original dataset 
// and reverse the edits you made. 
// For example, if you add certain weaknesses to Squirtle, this
// will make sure Squirtle is reset back to its original state 
// after you restard your server. 
pokeDataUtil.restoreOriginalData();

// Load contents of poke.json into global variable. 
var _DATA = pokeDataUtil.loadData().pokemon;

/// Setup body-parser. No need to touch this.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", function (req, res) {
    // HINT: Thanks Benny!
    var contents = "";
    _.each(_DATA, function (i) {
        contents += `<tr><td>${i.id}</td><td><a href="/pokemon/${i.id}">${i.name}</a></td></tr>\n`;
    })
    var html = `<html>\n<body>\n<table>${contents}</table>\n</body>\n</html>`;
    res.send(html);
});

app.get("/pokemon/:pokemon_id", function (req, res) {
    var contents = "";
    for (key in _DATA[req.params.pokemon_id - 1]) {
        contents += `<tr><td>${key}</td><td>${JSON.stringify(_DATA[req.params.pokemon_id - 1][key])}</td></tr>\n`;
    }
    var html = `<html>\n<body>\n<table>${contents}</table>\n</body>\n</html>`;

    res.send(html);
});

app.get("/pokemon/image/:pokemon_id", function (req, res) {
    res.send(`<img src = "${_DATA[req.params.pokemon_id - 1]["img"]}">`)
});

app.get("/api/id/:pokemon_id", function (req, res) {
    // This endpoint has been completed for you.  
    var _id = parseInt(req.params.pokemon_id);
    var result = _.findWhere(_DATA, { id: _id })
    if (!result) return res.json({});
    res.json(result);
});

app.get("/api/evochain/:pokemon_name", function (req, res) {
    //var pokId = parseInt(Object.keys(_DATA).filter(x => _DATA[x].name === req.params.pokemon_name));
    var result = _.findWhere(_DATA, { name: req.params.pokemon_name })
    if (!result) {
        return res.send([]);
    }
    var chain = []
    if (result["prev_evolution"]) result["prev_evolution"].map(x => chain.push(x.name));
    chain.push(req.params.pokemon_name);
    if (result["next_evolution"]) result["next_evolution"].map(x => chain.push(x.name));

    res.send(chain);

});

app.get("/api/type/:type", function (req, res) {
    var result = _.filter(_DATA, function (pok) { return pok.type.includes(req.params.type) })
    res.send(_.pluck(result, "name"))
});

app.get("/api/type/:type/heaviest", function (req, res) {
    var result = _.filter(_DATA, function (pok) { return pok.type.includes(req.params.type) })
    if (result.length === 0) return res.json({})
    result = _.max(result, pok => parseInt(pok.weight.split(" ")[0]))
    res.json({ "name": result.name, "weight": result.weight.split(" ")[0] })
});

app.post("/api/weakness/:pokemon_name/add/:weakness_name", function (req, res) {
    // HINT: 
    // Use `pokeDataUtil.saveData(_DATA);`
    //Figure out how to post to test
    var result = _.findWhere(_DATA, { name: req.params.pokemon_name })
    if (!result) return res.json({});
    pokID = result.id;
    if (!result.weaknesses.includes(req.params.weakness_name))
        _DATA[pokID - 1].weaknesses.push(req.params.weakness_name);
    pokeDataUtil.saveData(_DATA);
    res.json({ "name": result.name, "weaknesses": _DATA[pokID - 1].weaknesses })
});

app.delete("/api/weakness/:pokemon_name/remove/:weakness_name", function (req, res) {
    var result = _.findWhere(_DATA, { name: req.params.pokemon_name })
    if (!result) return res.json({});
    pokID = result.id;
    if (result.weaknesses.includes(req.params.weakness_name))
        _DATA[pokID - 1].weaknesses = _DATA[pokID - 1].weaknesses.filter(x => x !== req.params.weakness_name);
    pokeDataUtil.saveData(_DATA);
    res.json({ "name": result.name, "weaknesses": _DATA[pokID - 1].weaknesses })
});


// Start listening on port PORT
app.listen(PORT, function () {
    console.log('Server listening on port:', PORT);
});

// DO NOT REMOVE (for testing purposes)
exports.PORT = PORT
