var ctx = document.getElementById
    ("canvas").getContext("2d");
var heightpx;
var widthpx;
var height;
var width;

var squareSize = 10;//px
//Array that holds color of each square in colors[x][y]
var colors = [];
var toUpdate = [];
init();

var ball = { x: 1, y: 1 };
var vel = { x: 1, y: 1 };

function init() {
    heightpx = document.getElementById("canvas").height;
    widthpx = document.getElementById("canvas").width;
    height = Math.floor(heightpx / squareSize);
    width = Math.floor(widthpx / squareSize);
    for (let i = 0; i < width; i++) {
        colors.push([]);
        for (let j = 0; j < height; j++) {
            // colors[i].push({ r: i * 255 / width, g: j * 255 / height, b: 0 });
            colors[i].push({ r: 0, g: 0, b: 0 });
        }
    }
    draw(true);
    setInterval(() => {
        step();
    }, 100);
}

function step() {
    colors[ball.x][ball.y] = { r: 0, g: 0, b: 0 };
    toUpdate.push({ x: ball.x, y: ball.y });
    if (ball.x <= 0 || ball.x > width - 2) {
        vel.x *= -1;
    }
    if (ball.y <= 0 || ball.y > height - 2) {
        vel.y *= -1;
    }
    ball.x += vel.x;
    ball.y += vel.y;
    colors[ball.x][ball.y] = { r: 50, g: 50, b: 50 };
    toUpdate.push({ x: ball.x, y: ball.y });
    draw(false);

}

function draw(drawAll) {
    if (drawAll) {
        for (let i = 0; i < colors.length; i++) {
            for (let j = 0; j < colors[i].length; j++) {
                let c = colors[i][j];
                ctx.fillStyle = `rgba(${c.r},${c.g},${c.b},1)`
                ctx.fillRect(i * squareSize, j * squareSize, squareSize, squareSize);
            }
        }
    }
    else {
        for (let o of toUpdate) {
            let c = colors[o.x][o.y];
            ctx.fillStyle = `rgba(${c.r},${c.g},${c.b},1)`
            ctx.fillRect(o.x * squareSize, o.y * squareSize, squareSize, squareSize);
        }
        toUpdate = [];
    }
}



