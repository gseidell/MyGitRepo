var ctx = document.getElementById
    ("canvas").getContext("2d");
var heightpx;
var widthpx;
var height;
var width;

var squareSize = 24;//px
//Array that holds color of each square in colors[x][y]
var colors = [];
var toUpdate = [];

var p1 = { x: 0, y: 0, xm: 1, ym: 0, boost: 0 };
var p2 = { x: 0, y: 0, xm: -1, ym: 0, boost: 0 };
var food = { x: 0, y: 0 };
var p1Color = { r: 51, g: 153, b: 51 };
var p2Color = { r: 26, g: 83, b: 255 };
var deadColor = { r: 255, g: 51, b: 51 };
var foodColor = { r: 0, g: 255, b: 0 };
var boostColor = { r: 255, g: 255, b: 255 };

var interval = 0;

function resetPlayers() {
    p1.x = -1;
    p1.y = Math.floor(height / 2);
    p2.x = width;
    p2.y = Math.floor(height / 2);

    p1.xm = 1;
    p1.ym = 0;
    p2.xm = -1;
    p2.ym = 0;

    p1.boost = 0;
    p2.boost = 0;
}
function placeFood() {
    var loc = { x: 0, y: 0 };
    do {
        loc.x = Math.floor(Math.random() * width)
        loc.y = Math.floor(Math.random() * height)
    } while (!colorEqual(colors[loc.x][loc.y], { r: 0, g: 0, b: 0 }));
    food.x = loc.x;
    food.y = loc.y;
    colors[food.x][food.y] = foodColor;
    toUpdate.push({ x: food.x, y: food.y }); me
}

function colorEqual(a, b) {
    return a.r == b.r &&
        a.g == b.g &&
        a.b == b.b;
}

function init() {
    colors = [];
    heightpx = document.getElementById("canvas").height;
    widthpx = document.getElementById("canvas").width;
    height = Math.floor(heightpx / squareSize);
    width = Math.floor(widthpx / squareSize);

    resetPlayers();
    document.getElementById("left").classList.remove("big");
    document.getElementById("right").classList.remove("big");


    for (let i = 0; i < width; i++) {
        colors.push([]);
        for (let j = 0; j < height; j++) {
            // colors[i].push({ r: i * 255 / width, g: j * 255 / height, b: 0 });
            colors[i].push({ r: 0, g: 0, b: 0 });
        }
    }

    draw(true);
    interval = setInterval(step, 75);
    placeFood();
}

function step() {

    p1.x += p1.xm;
    p1.y += p1.ym;
    p2.x += p2.xm;
    p2.y += p2.ym;

    let win = [false, false];

    if (p1.x < 0 || p1.x > width - 1 || p1.y < 0 || p1.y > height - 1) {
        if (p1.boost > 0) {
            if (p1.x < 0 || p1.x > width - 1) {
                p1.x = (p1.x + width) % width
            }                                       //wraparound
            if (p1.y < 0 || p1.y > height - 1) {
                p1.y = (p1.y + height) % height
            }
        } else {
            win[1] = true;
        }
    }
    if (!win[1] && !colorEqual(colors[p1.x][p1.y], { r: 0, g: 0, b: 0 })) {
        if (colorEqual(colors[p1.x][p1.y], foodColor)) {
            p1.boost += 40;
            placeFood();
        } else if (p1.boost == 0) {
            win[1] = true;
        }
    }

    if (p2.x < 0 || p2.x > width - 1 || p2.y < 0 || p2.y > height - 1) {
        if (p2.boost > 0) {
            if (p2.x < 0 || p2.x > width - 1) {
                p2.x = (p2.x + width) % width
            }                                       //wraparound
            if (p2.y < 0 || p2.y > height - 1) {
                p2.y = (p2.y + height) % height
            }
        } else {
            win[0] = true;

        }
    }
    if (!win[0] && !colorEqual(colors[p2.x][p2.y], { r: 0, g: 0, b: 0 })) {
        if (colorEqual(colors[p2.x][p2.y], foodColor)) {
            p2.boost += 40;
            placeFood();
        } else if (p2.boost == 0) {
            win[0] = true;
        }
    }


    if (win[0] || win[1]) {
        clearInterval(interval);
        interval = -1;
        if (win[0]) {
            if (typeof colors[p2.x] != "undefined" &&
                typeof colors[p2.x][p2.y] != "undefined") {
                colors[p2.x][p2.y] = deadColor;
                toUpdate.push({ x: p2.x, y: p2.y });
            }
            document.getElementById("left").innerHTML =
                Number(document.getElementById("left").innerHTML) + 1;
            document.getElementById("left").classList.add("big");

        }
        if (win[1]) {
            if (typeof colors[p1.x] != "undefined" &&
                typeof colors[p1.x][p1.y] != "undefined") {
                colors[p1.x][p1.y] = deadColor;
                toUpdate.push({ x: p1.x, y: p1.y });
            }
            document.getElementById("right").innerHTML =
                Number(document.getElementById("right").innerHTML) + 1;
            document.getElementById("right").classList.add("big");
        }
        draw(false);
        return;
    }

    colors[p1.x][p1.y] = p1.boost == 0 ? p1Color : boostColor;
    toUpdate.push({ x: p1.x, y: p1.y });
    colors[p2.x][p2.y] = p2.boost == 0 ? p2Color : boostColor;
    toUpdate.push({ x: p2.x, y: p2.y });

    p1.boost = Math.max(p1.boost - 1, 0);
    p2.boost = Math.max(p2.boost - 1, 0);

    draw(false);


}

function draw(drawAll) {
    if (drawAll) {
        for (let i = 0; i < colors.length; i++) {
            for (let j = 0; j < colors[i].length; j++) {
                let c = colors[i][j];
                ctx.fillStyle = `rgba(${c.r},${c.g},${c.b},1)`
                ctx.fillRect(i * squareSize, j * squareSize, squareSize, squareSize);
            }
        }
    }
    else {
        for (let o of toUpdate) {
            let c = colors[o.x][o.y];
            ctx.fillStyle = `rgba(${c.r},${c.g},${c.b},1)`
            ctx.fillRect(o.x * squareSize, o.y * squareSize, squareSize, squareSize);
        }
        toUpdate = [];
    }
}

window.onkeydown = function (event) {
    if (event.key == " ") {
        if (interval == -1) {
            console.log("hi")

            init();
        }
    }
    switch (event.key) {
        case "w":
            if (p1.ym == 0) {
                p1.xm = 0;
                p1.ym = -1;
            }
            break;
        case "a":
            if (p1.xm == 0) {
                p1.xm = -1;
                p1.ym = 0;
            }
            break;
        case "s":
            if (p1.ym == 0) {
                p1.xm = 0;
                p1.ym = 1;
            }
            break;
        case "d":
            if (p1.xm == 0) {
                p1.xm = 1;
                p1.ym = 0;
            }
            break;

        case "ArrowUp":
            if (p2.ym == 0) {
                p2.xm = 0;
                p2.ym = -1;
            }
            break;
        case "ArrowLeft":
            if (p2.xm == 0) {
                p2.xm = -1;
                p2.ym = 0;
            }
            break;
        case "ArrowDown":
            if (p2.ym == 0) {
                p2.xm = 0;
                p2.ym = 1;
            }
            break;
        case "ArrowRight":
            if (p2.xm == 0) {
                p2.xm = 1;
                p2.ym = 0;
            }
            break;
    }
};

