var ctx = document.getElementById
    ("canvas").getContext("2d");

var squareSize = 2;//px
//Array that holds color of each square in colors[x][y]
var colors = [];
init();

function init() {
    for (let i = 0; i < 500 / squareSize; i++) {
        colors.push([]);
        for (let j = 0; j < 500 / squareSize; j++) {
            colors[i].push({ r: i * 255 / 250, g: j * 255 / 250, b: 0 });
        }
    }
}

function draw() {
    for (let i = 0; i < colors.length; i++) {
        for (let j = 0; j < colors[i].length; j++) {
            let c = colors[i][j];
            ctx.fillStyle = `rgba(${c.r},${c.g},${c.b},1)`
            ctx.fillRect(i * squareSize, j * squareSize, squareSize, squareSize);
        }
    }

}

