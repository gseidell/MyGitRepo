

function Node(data) {
    this.data = data;
    this.next = null;
    this.prev = null;
}

function DoubleLL(comparator = function () { }) {
    this.head = null;
    this.tail = null;
    this.size = 0;
    this.comparator = comparator;
}

function addFront(data) {
    let add = new Node(data);
    if (this.head == null) {
        this.head = add;
        this.tail = add;
    }
    else {
        this.head.prev = add;
        add.next = this.head;
        this.head = add;
    }
    this.size++;
}
function addBack(data) {
    let add = new Node(data);
    if (this.tail == null) {
        this.head = add;
        this.tail = add;
    }
    else {
        this.tail.next = add;
        add.prev = this.tail;
        this.tail = add;
    }
    this.size++;
}
function addAfter(data, index) { //aint working
    if (this.head == null) {
        addFront(data);
        return;
    }
    if (index >= this.size - 1) {
        addBack(data);
        return;
    }
    let add = new Node(data);
    let current = this.head;
    let count = 0;
    while (index != count) {
        current = current.next;
        count++;
    }
    add.next = current;
    add.prev = current.prev;
    current.prev = add;
    this.size++;

}
function print() {
    if (this.head == null)
        return "Empty";
    let curr = this.head;
    let result = "";
    while (curr != null) {
        result += curr.data + "|";
        curr = curr.next
    }
    return result.substring(0, result.length - 1);
}

DoubleLL.prototype = {
    constructor: DoubleLL,
    addFront: addFront,
    addBack: addBack,
    addAfter: addAfter,
    print: print,
    getSize: function () { return this.size; },
    isEmpty: function () { return this.size == 0; }
}