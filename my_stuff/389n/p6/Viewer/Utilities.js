
function Utility(utilityName, utilityDescription) {
    this.utilityDescription = utilityDescription;
    this.utilityName = utilityName;
}

Utility.prototype = {
    constructor: Utility,
    info: function () {
        console.log("Name: " + this.utilityName);
        console.log("Description: " + this.utilityDescription)
    }
};

function Viewer(name, description) {
    Utility.call(this, name, description);
    this.list = new DoubleLL();
}
Viewer.prototype = new Utility();
Viewer.prototype.constructor = Viewer;
Viewer.prototype.current = null;
Viewer.prototype.getArrayPhotosNames = function () {
    let folder = document.getElementById("folder").value;
    let common = document.getElementById("common").value;
    let start = parseInt(document.getElementById("startNum").value);
    let end = parseInt(document.getElementById("endNum").value);
    this.list = new DoubleLL(function () { });
    for (let i = start; i <= end; i++) {
        let fileName = folder + common + i + ".jpg";
        this.list.addBack(fileName);
    }
    this.current = this.list.head;
};
Viewer.prototype.next = function () {
    if (this.current != null) {
        if (this.current.next != null) {
            this.current = this.current.next;
            return this.current.data;
        }
        else {
            this.current = this.list.head;
            return this.current.data;
        }
    }
};
Viewer.prototype.prev = function () {
    if (this.current != null) {
        if (this.current.prev != null) {
            this.current = this.current.prev;
            return this.current.data;
        }
        else {
            this.current = this.list.tail;
            return this.current.data;
        }
    }
};


let myViewer = new Viewer("viewer", "that views");


function Recorder(name, description) {
    Utility.call(this, name, description);
    this.list = new DoubleLL();
}
Recorder.prototype = new Utility();
Recorder.prototype.constructor = Recorder;
Recorder.prototype.current = null;
Recorder.prototype.log = function (data) {
    this.list.addBack(data);
};
Recorder.prototype.save = function () {
    let name = document.getElementById("saveName").value;
    //console.log(this.list == null);
    localStorage.setItem(name, myRecorder.list.print());
};
Recorder.prototype.retrieve = function () {
    let name = document.getElementById("saveName").value;
    myRecorder.list = new DoubleLL();
    let data = localStorage.getItem(name).split('|');
    for(let i=0;i!=data.length;i++){
        myRecorder.list.addBack(data[i]);
    }
    myRecorder.current= myRecorder.list.head;

};
Recorder.prototype.next = function () {
    if (this.current == null) {
        this.current = this.list.head;
    }
    if (this.current.next != null) {
        this.current = this.current.next;
        return this.current.data;
    }
    else {
        this.current = this.list.head;
        return "done";
    }

};

let myRecorder = new Recorder("recorder", "that records");