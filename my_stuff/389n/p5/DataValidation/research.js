window.onsubmit = validateForm;

function validateStudyId() {
    var id1 = document.getElementsByName("firstFourDigits")[0].value;
    var id2 = document.getElementsByName("secondFourDigits")[0].value;

    return !(id1.charAt(0) != 'A' || isNaN(id1.substring(1, id1.length)) || id1.length != 4 ||
        id2.charAt(0) != 'B' || isNaN(id2.substring(1, id2.length)) || id2.length != 4);
        

}

function validateForm() {

    var phone1 = document.getElementsByName("phoneFirstPart")[0].value;
    var phone2 = document.getElementsByName("phoneSecondPart")[0].value;
    var phone3 = document.getElementsByName("phoneThirdPart")[0].value;

    var invalidMessages = "";
    if (isNaN(phone1) || phone1.length != 3 || 
        isNaN(phone2) || phone2.length != 3 || 
        isNaN(phone3) || phone3.length != 4) {
        invalidMessages += "Invalid phone number.\n";
    }
    

    var condition =
        document.getElementsByName("highBloodPressure")[0].checked ||
        document.getElementsByName("diabetes")[0].checked ||
        document.getElementsByName("glaucoma")[0].checked ||
        document.getElementsByName("asthma")[0].checked;

    var none = document.getElementsByName("none")[0].checked;
    if ((condition && none) || (!condition && !none))
        invalidMessages += "Invalid conditions selection.\n";

    var period = document.getElementsByName("period");
    var flag = false; 
    for (var idx = 0; idx < period.length; idx++) {
        if (period[idx].checked) {
            flag = true;
        }
    }
    if (!flag)
        invalidMessages += "No time period selected.\n";
    if (!validateStudyId())
        invalidMessages += "Invalid study id.\n";

    if (invalidMessages !== "") {
        alert(invalidMessages);
        return false;
    }
    else {
        return (confirm("Do you want to submit the form data?"));
    }


}