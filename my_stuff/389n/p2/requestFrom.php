<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <!-- For responsive page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Basic HTML5 Document" />
    <meta name="keywords" content="HTML5, Responsive" />
    <link href="favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Order Form</title>
</head>

<body>
    <div class="container-fluid">
        <h1>Order Request Form</h1>
        <form action="processRequest.php" method="post">
            <p>
                <div class="form-group">

                    <h4>Name</h4>
                    <div class="col-sm-4">
                        <input type="text" name="name" class="form-control">
                    </div>
                    <br>
                    <br>
                    <h4>Email</h4>
                    <div class="col-sm-4">
                        <input type="text" name="email" class="form-control">
                    </div>
                    <br>
                    <br>
                    <h4>CellPhone (XXX)XXX-XXXX</h4>
                    <div class="col-sm-4">
                        <input type="text" name="phone" class="form-control" pattern = '\(\d{3}\)\d{3}-\d{4}'>
                    </div>

                    <br>
                    <br>
                    <h3>Shipping Method</h3>
                    <strong>USPS</strong>
                    <input type="radio" name="shipping" value="USPS" checked="checked" />&nbsp;
                    <strong>FedEx</strong>
                    <input type="radio" name="shipping" value="FedEX" />&nbsp;
                    <strong>UPS</strong>
                    <input type="radio" name="shipping" value="UPS" />&nbsp;
                </div>


                <h3>Software</h3>
                <div class="form-group">

                    <select name="software[]" multiple="multiple">
                        <?php
                    include("softwares.php");
                    $keys = array_keys($softwares);
                    foreach ($softwares as $key => $value) {
                        createSelectionLine($key, $value);
                    }
                ?>
                    </select>
                </div>
                <h3>Order Specifications</h3>
                <textarea rows="5" cols="80" name="specs"></textarea>	
            </p>
            <p>
                <input type="reset" />
                <input type="submit" />
            </p>
        </form>
    </div>
</body>

</html>

<?php
    function createSelectionLine($name, $price)
    {
        print "<option value=\"$name\" selected = \"selected\">$name  (\$$price)</option>";
    }
