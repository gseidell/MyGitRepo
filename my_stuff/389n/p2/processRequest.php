<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <!-- For responsive page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Basic HTML5 Document" />
    <meta name="keywords" content="HTML5, Responsive" />
    <link href="favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Order Summary</title>
</head>

<body>
    <div class = "container-fluid">
        <h1>Order Confirmation</h1>
    <?php 
            include("softwares.php");
            $nameSubmitted = trim($_POST['name']);
            $emailSubmitted = trim($_POST['email']);
            $phoneSubmitted = $_POST['phone'];
            $shippingMethod = $_POST['shipping'];
            $specsSubmitted = $_POST['specs'];
            echo "<strong>Name : $nameSubmitted</strong><br>";
            echo "<strong>Email : $emailSubmitted</strong><br>";
            echo "<strong>Phone : $phoneSubmitted</strong><br>";
            echo "<strong>Shipping Method : $shippingMethod</strong><br>";

            $sum = 0;
            print('<h3>Order Summary</h3>');
            foreach ($_POST["software"] as $entry) {
                print('<div class = "row">');
                print('<div class="col-sm-1"></div>');            

                print('<div class="col-sm-2">');            
                print($entry);
                print("</div>");
                print('<div class="col-sm-2">');   
                print '$';         
                print($softwares[$entry]);	
                print("</div>");
               
                print("</div>");
                $sum += $softwares[$entry];
            }
        ?>
            <div class = "row">
                <div class = col-sm-1></div>
                <div class = "col-sm-2"><strong>Total</strong></div>
                <div class = "col-sm-2">
                    <strong>
                    <?php
                        print '$';
                        print $sum;
                    ?>
                    </strong>
                </div>
            </div>
            
            <h3>Order Specifications</h3>
            <p>
                <?php
                    print $specsSubmitted;
                ?>
            </p>
        </div>
    

</body>

</html>