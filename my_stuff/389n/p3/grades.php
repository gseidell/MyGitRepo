<?php
    require_once("support.php");
    require_once("student.php");

    session_start();
    $body = "";
    $names = "";
    $fp = fopen($_SESSION["file"], "r") or die("Could not open file");
    $body.='<h2>Grades Submission Form</h2><form action = "submit.php" method = "post">';
    while (!feof($fp)) {
        $line = trim(fgets($fp));
        //$body .= "$line<br />";

        $names.=$line.",";

        $body.=createTableRow($line);
    }
    
    fclose($fp);
    $body .= '<input type="submit" />';
    $body.= "</form>";
    $page = generatePage($body);

    $expiration = time() + 3600; /* one hour from now */
    $path = "/"; /* a cookie should be sent for any page within the server environment */
    $domain = "localhost";  /* adjust with appropriate domain name */
    $sentOnlyOverSecureConnection = 0; /* 1 for secure connection */    
    setcookie("names", $names, $expiration, $path, $domain, 0);

    echo $page;
    
    
       
    
    
    function createTableRow($name)
    {
        $row = <<<BOOTSTRAP
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-2">
                $name
            </div>
            <div class="col-sm-1">
                A <input type="radio" name="$name" value="A" />
            </div>
            <div class="col-sm-1">
                B <input type="radio" name="$name" value="B" />
            </div>
            <div class="col-sm-1">
                C <input type="radio" name="$name" value="C" />
            </div>
            <div class="col-sm-1">
                D <input type="radio" name="$name" value="D" />
            </div>
            <div class="col-sm-1">
                F <input type="radio" name="$name" value="F" />
            </div>
        </div>
BOOTSTRAP;
        return $row;
    }
