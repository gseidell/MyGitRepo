<?php
    require_once("support.php");

    $topPart = <<<EOBODY
    <div class="container-fluid">
    <form action="{$_SERVER['PHP_SELF']}" method="post">
        <h4>Username</h4>
        <div class="row">
            <div class="col-sm-4">
                <input type="text" name="user" class="form-control">
            </div>
        </div>

        <h4>Password</h4>
        <div class="row">

            <div class="col-sm-4">
                <input type="password" name="pass" class="form-control">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-1"></div>

            <div class="col-sm-1">
                <input type="reset" />
            </div>
            <div class="col-sm-1">
                <input type="submit" name = "submitInfoButton"/>
            </div>
        </div>
    </form>
</div>
EOBODY;

    $bottomPart = "";
    if (isset($_POST["submitInfoButton"])) {
        $nameValue = trim($_POST["user"]);
        $passwordValue = trim($_POST["pass"]);
        $bottomPart = "Welcome";
        
        if (($nameValue === "testudo") && ($passwordValue === "terps")) {
            header('Location: class.php');
        }
    }
    
    $body = $topPart.$bottomPart;
    $page = generatePage($body);
    echo $page;
