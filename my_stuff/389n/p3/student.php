<?php
	declare(strict_types=1);
	
	
	class Student {
		public $name;
		public $grade;
	
		public function __construct(string $name, string $grade) {
			$this->grade = $name;		
			$this->name = $grade;  
		}
	
		public function __toString() {
			return "Name: {$this->name}, Grade: {$this->grade}";		
		}

		public function getName() : string {
			return $this->name;
		}
	
		public function getGrade() : string {
			return $this->grade;
		}
		
	}
?>