<?php
    require_once("support.php");


    $expiration = time() + 3600; /* one hour from now */
    $path = "/"; /* a cookie should be sent for any page within the server environment */
    $domain = "localhost";  /* adjust with appropriate domain name */
    $submissions = 1;
    if (!isset($_COOKIE['submissions'])) {       
        // First cookie
        setcookie("submissions", 1, $expiration, $path, $domain, 0);
    }
    else {
        $submissions = $_COOKIE["submissions"] + 1;
        setcookie("submissions", $submissions, $expiration, $path, $domain, 0);
    }
    
    $body = <<<PAGE
<h1>Grades Submitted</h1>
<h2>This is submission #$submissions</h2>  
<form action= "class.php">
    <input type = "submit" value = "Enter Grades for Another Section"/>
</form>
PAGE;

    $page = generatePage($body);
    echo $page;



    ?>