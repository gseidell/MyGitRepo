<?php
    require_once("support.php");
    require_once("student.php");

    session_start();
    $body = "";	    
    $names[] = explode(',',$_COOKIE['names'],-1);
    $grades[] = array();
    foreach ($names[0] as $key => $value) {
        if(!isset($_POST[$value]))
            $grades[$value] = "NGR";
        else {
            $grades[$value] = $_POST[$value];
        }
    }
    array_shift($grades);

    print_r($grades);
    print '<br>';
    $students[] = [];
    foreach ($grades as $key => $value) {
        $students[$key] = new student($value,$key);
    }
    array_shift($students);

    $body.='<h2>Grades to Submit</h2><form action = "submission.php" method = "post">';
    foreach ($students as $key => $value) {
        $body.=createTableRow($value);
    }

    $body .= '<input type="submit" />';
    $body.= "</form>";
    $body.= '<form action = "grades.php"><input type="submit" value = "Back"/></form>';
    $page = generatePage($body);
    echo $page;

    
    function createTableRow($student)
    {
        $row = "
        <div class=\"row\">
            <div class=\"col-sm-1\">
            </div>
            <div class=\"col-sm-2\">".
                $student->getName()."
            </div>
            <div class=\"col-sm-1\">".
                $student->getGrade()."    
            </div>
           
        </div>
";
        return $row;
    }
    ?>