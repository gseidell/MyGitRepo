<?php
    require_once("support.php");

    $topPart = <<<EOBODY
<div class="container-fluid">
    <form action="{$_SERVER['PHP_SELF']}" method="post">
        <h4>Course Name:</h4>
        <div class="row">
            <div class="col-sm-4">
                <input type="text" name="course" class="form-control" placeholder = "CMSC389N">
            </div>
        </div>

        <h4>Section:</h4>
        <div class="row">

            <div class="col-sm-4">
                <input type="text" name="section" class="form-control" placeholder = "0101">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-1"></div>

            <div class="col-sm-1">
                <input type="reset" />
            </div>
            <div class="col-sm-1">
                <input type="submit" name = "submitInfoButton"/>
            </div>
        </div>
    </form>
</div>
EOBODY;

    $bottomPart = "";
    if (isset($_POST["submitInfoButton"])) {
        $courseValue = trim($_POST["course"]);
        $sectionValue = trim($_POST["section"]);
        
        $fileName = $courseValue.$sectionValue.".txt";

        if (file_exists($fileName)) {
            session_start();
            $_SESSION["file"] = $fileName;
            header('Location: grades.php');
        }
        
    }
    
    $body = $topPart.$bottomPart;
    $page = generatePage($body);
    echo $page;
