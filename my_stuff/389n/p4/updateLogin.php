<?php
    require_once("support.php");
    require_once("dbLogin.php");
    
    session_start();


    $body = <<<PAGE
    <form action = "updateLogin.php" method="POST">
        <b>Email associated with application: <b><input type="text" name="email" class="form-control" required>
        <b>Password associated with application: <b><input type="password" name="password" class="form-control" required>
        <input type="submit">
        <input type="button" value="Return to main menu" onclick='window.location.href = "main.html"'>
    </form>  
PAGE;

    if (isset($_POST['email']) && isset($_POST['password'])) {
        $db_connection = new mysqli($host, $user, $password, $database);
        if ($db_connection->connect_error) {
            die($db_connection->connect_error);
        } else {
            //echo "Connection to database established<br><br>";
        }
        $emailSubmitted = trim($_POST['email']);
        $passSubmitted = trim($_POST['password']);
        
        $query = "select * from applicants where email = '{$emailSubmitted}';"; //and password = '{$passSubmitted}';";
        $result = $db_connection->query($query);
        if (!$result) {
            die("Retrieval failed: ". $db_connection->error);
        } else {
            /* Number of rows found */
            $num_rows = $result->num_rows;
            if ($num_rows === 0) {
                $body.='<br>No entry exists in the database for the specified email and password';
            } else {
                $result->data_seek(0);
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                if(!password_verify($passSubmitted,$row['password'])){
                    $body.='No entry exists in the database for the specified email and password';
                }
                else{
                    $result->data_seek(0);
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                
                    $_SESSION['name'] = $row['name'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['gpa'] = $row['gpa'];
                    $_SESSION['year'] = $row['year'];
                    $_SESSION['gender'] = $row['gender'];

                    header("Location: updateApplication.php");
                }
            }
        }

    }




    echo generatePage($body);
