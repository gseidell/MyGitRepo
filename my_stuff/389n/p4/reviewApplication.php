<?php
    require_once("support.php");
    require_once("dbLogin.php");
    
    session_start();


    $body = "";

    if (isset($_POST['email']) && isset($_POST['password'])) {
        $db_connection = new mysqli($host, $user, $password, $database);
        if ($db_connection->connect_error) {
            die($db_connection->connect_error);
        } else {
            //echo "Connection to database established<br><br>";
        }
        $emailSubmitted = trim($_POST['email']);
        $passSubmitted = trim($_POST['password']);
        
        $query = "select * from applicants where email = '{$emailSubmitted}';"; //and password = '{$passSubmitted}';";
        $result = $db_connection->query($query);
        if (!$result) {
            die("Retrieval failed: ". $db_connection->error);
        } else {
            /* Number of rows found */
            $num_rows = $result->num_rows;
            if ($num_rows === 0) {
                $body.='No entry exists in the database for the specified email and password';
                $body .= '<form action= "main.html">
                    <input type = "submit" value = "Return to the main menu"/>
                    </form>';
            } else {
                    $result->data_seek(0);
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                if(!password_verify($passSubmitted,$row['password'])){
                    $body.='No entry exists in the database for the specified email and password';
                    $body .= '<form action= "main.html">
                    <input type = "submit" value = "Return to the main menu"/>
                    </form>';
                }
                else {
                    //$result->data_seek(0);
                    //$row = $result->fetch_array(MYSQLI_ASSOC);
                    $body .= <<<PAGE
                    <h3>Application found in the database with the following values: </h3>
                    <b>Name: </b>{$row['name']}
                    <br>
                <b>Email: </b>{$row['email']}
                <br>
        
                <b>Gpa: </b>{$row['gpa']}
                <br>
        
                <b>Year: </b>{$row['year']}
                <br>
                
                <b>Gender: </b>{$row['gender']}
                <br>
                
                <form action= "main.html">
                <input type = "submit" value = "Return to the main menu"/>
                </form>
PAGE;
                
                
                }
            }
        }

    }
    else {
        $body = <<<PAGE
    <form action = "reviewApplication.php" method="POST">
        <b>Email associated with application: <b><input type="text" name="email" class="form-control" required>
        <b>Password associated with application: <b><input type="password" name="password" class="form-control" required>
        <input type="submit">
        <input type="button" value="Return to main menu" onclick='window.location.href = "main.html"'>
    </form>  
PAGE;
    }

    echo generatePage($body);

