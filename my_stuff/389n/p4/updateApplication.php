<?php

require_once("support.php");
    require_once("dbLogin.php");
    
    session_start();
    $name = trim($_SESSION['name']);
    $email = trim($_SESSION['email']);
    $gpa = trim($_SESSION['gpa']);
    $year = $_SESSION['year'];
    $gender = $_SESSION['gender'];

$body = <<<PAGE
    <div class="container-fluid">
        <form action="confirmUpdate.php" method="post" onsubmit="return passwordCheck()">
            <p>
                <div class="form-group">
                    <h4>Name</h4>
                    <div class="col-sm-4">
                        <input type="text" name="name" class="form-control" required value = "{$name}">
                    </div>
                    <br>
                    <br>
                    <h4>Email</h4>
                    <div class="col-sm-4">
                        <input type="text" name="email" class="form-control" required value = "{$email}">
                    </div>
                    <br>
                    <br>
                    <h4>GPA</h4>
                    <div class="col-sm-4">
                        <input type="number" name="gpa" class="form-control" required value = "{$gpa}">
                    </div>

                    <br>
                    <br>
                    <h3>Year</h3>
                    <strong>10</strong>
PAGE;
                    $body.=
                    '<input type="radio" name="year" value="10"';
                    if($year == "10")
                        $body.= 'checked="checked" />';
                    $body.="&nbsp;";
                    $body.='
                    <strong>11</strong>
                    <input type="radio" name="year" value="11"';
                    if($year == "11")
                        $body.= 'checked="checked" />';
                    $body.="&nbsp;";
                    $body.='
                    <strong>12</strong>
                    <input type="radio" name="year" value="12"';
                    if($year == "12")
                        $body.= 'checked="checked" />';
                    $body.="&nbsp;";

                    $body.='<br>
                    <h3>Gender</h3>
                    <strong>M</strong>
                    <input type="radio" name="gender" value="M"';
                    if($gender == "M")
                        $body.= 'checked="checked" />';
                    $body.='&nbsp;
                    <strong>F</strong>
                    <input type="radio" name="gender" value="F"';
                    if($gender == "F")
                        $body.= 'checked="checked" />';
                    $body.='&nbsp';

$body.= <<<PAGE
                    <br>
                    <br>
                    <h4>Password</h4>
                    <div class="col-sm-4">
                        <input type="password" name="password" id="p1" class="form-control" required>
                    </div>

                    <br>
                    <br>
                    <h4>Verify Password</h4>
                    <div class="col-sm-4">
                        <input type="password" name="passwordVerify" id="p2" class="form-control" required>
                    </div>

                    <br>
                    <br>
                    <br>
                    <input type="submit">
                    <br>
                    <br>
                    <input type="button" value="Return to main menu" onclick='window.location.href = "main.html"'>


                </div>
            </p>
        </form>

        <script>
           
            function passwordCheck() {
                var pass1 = document.getElementById("p1").value;
                var pass2 = document.getElementById("p2").value;

                if (pass1 == "" || pass1 != pass2){
                    alert("passwords must match")
                    return false;
                }

            }

        </script>

    </div>


PAGE;

echo generatePage($body);
?>
