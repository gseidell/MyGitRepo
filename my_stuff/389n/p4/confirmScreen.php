<?php
    require_once("support.php");
    require_once("dbLogin.php");
    
    session_start();


        $nameSumbitted = trim($_POST['name']);
        $emailSumbitted = trim($_POST['email']);
        $gpaSumbitted = trim($_POST['gpa']);
        $passwordSumbitted = password_hash($_POST['password'], PASSWORD_DEFAULT);

        $genderSubmitted = $_POST['gender'];
        $yearSubmitted = $_POST['year'];

        $db_connection = new mysqli($host, $user, $password, $database);
        if ($db_connection->connect_error) {
            die($db_connection->connect_error);
        } else {
            //echo "Connection to database established<br><br>";
        }
        
        $query = "insert into applicants values('{$nameSumbitted}', '{$emailSumbitted}', {$gpaSumbitted}, {$yearSubmitted}, '{$genderSubmitted}', '{$passwordSumbitted}')";
        //echo $query;
        $result = $db_connection->query($query);

        if (!$result) {
            die("Insertion failed: " . $db_connection->error);
        } else {
            //echo "Insertion completed.<br>";
        }

        $body = <<<PAGE
        <h3>The following entry has been added to the database</h3>
        <b>Name: </b>{$nameSumbitted}
        <br>
        <b>Email: </b>{$emailSumbitted}
        <br>

        <b>Gpa: </b>{$gpaSumbitted}
        <br>

        <b>Year: </b>{$yearSubmitted}
        <br>

        <b>Gender: </b>{$genderSubmitted}
        <br>

        <form action= "main.html">
            <input type = "submit" value = "Return to the main menu"/>
        </form>
PAGE;

        echo generatePage($body);
