<?php
          require_once("support.php");
          require_once("dbLogin.php");
          
    $fields = array("name", "email", "gpa",
        "year", "gender");

$body = '<style>
table, td, th { border: 1px solid gray;padding:3px }
</style> ';
$body .= <<<PAGE
        <form action = "administrative.php" method="POST">
        <h1>Applications</h1>
        <h3>Select fields to diaplay</h3>
PAGE;
    $body .= '    
                <div class="form-group">
                    <select name="selectedFields[]" multiple="multiple" required>';
                    foreach ($fields as $value) {
                        $body.=createSelectionLine($value);
                    }
    $body.='
                    </select>
                </div>
';
$body .= '
<h3>Select field to sort by</h3>
<div class="form-group">
                    <select name="sort">';
                    foreach ($fields as $value) {
                        $body.=createSelectionLine($value);
                    }
    $body.='
                    </select>
                </div>
';


$body .= '
    <h3>Filter Condition</h3>
    <input type = "text" name = "filter"/>
    <br/>
    <br/>
';

$body .= <<<PAGE
<input type="submit" name = "submit"/>
<input type="button" value="Return to main menu" onclick='window.location.href = "main.html"'/>
</form>
<br>
<br>
PAGE;
    //print_r ($_POST);
    if (isset($_POST['submit'])) {

        $db_connection = new mysqli($host, $user, $password, $database);
        if ($db_connection->connect_error) {
            die($db_connection->connect_error);
        } else {
            //$body.= "Connection to database established<br><br>";
        }
        $fieldsSubmitted = implode(', ',$_POST['selectedFields']);
        $sortSubmitted = $_POST['sort'];
        $filter = trim($_POST['filter']);
        if($filter == "")
            $filter = 'true';
        
        
        $query = "select {$fieldsSubmitted} from applicants where {$filter} order by {$sortSubmitted};";
        //echo $query;


        $result = $db_connection->query($query);
        if (!$result) {
            die("Retrieval failed: ". $db_connection->error);
        } else {
            $num_rows = $result->num_rows;
            if ($num_rows === 0) {
                echo "Empty Table<br>";
            } else {
                $body .= "<table><thead><tr>";
                    foreach($_POST['selectedFields'] as $key => $value){
                        $body.= "<th>{$value}</th>";
                    }

                $body .= "</tr></thead>";
                for ($row_index = 0; $row_index < $num_rows; $row_index++) {                    
                    $result->data_seek($row_index);
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $body .= "<tr>";
                    foreach ($row as $key => $value) {
                        $body.="<td>{$value}</td>";
                    }
                    $body .= "</tr>";
                }
                $body .= "</table>";
            }
        }
    }


    echo generatePage($body);
function createSelectionLine($field)
{
    return "<option value=\"$field\">$field</option>";
}
