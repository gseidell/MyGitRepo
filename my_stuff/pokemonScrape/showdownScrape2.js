var regexP1 = RegExp("^\\|.{0,6}\\|p1a");
var regexP2 = RegExp("^\\|.{0,6}\\|p2a");
var regexMove = RegExp("^\\|move\\|");
var regexSwitch = RegExp("^\\|switch\\|");
var regexP1Pokemon = RegExp("switch\\|p1a: (\\w*)");
var regexP2Pokemon = RegExp("switch\\|p2a: (\\w*)");

// var regexP2PokemonAndMove = RegExp("move\\|p2a: ([a-zA-Z -]*)\\|([a-zA-Z -]*)\\|");
var regexP2PokemonAndMove = RegExp("move\\|p2a: ([a-zA-Z]*)\\|([a-zA-Z\\s-]*)\\|");

var test;

function scrapeLog(log) {
	var p1 = log.filter(line => regexP1.test(line));
	var p1Moves = p1.filter(line => regexMove.test(line) || regexSwitch.test(line));
	var p2 = log.filter(line => regexP2.test(line));
	var p2Moves = p2.filter(line => regexMove.test(line));
	var p2Switches = p2.filter(line => regexSwitch.test(line));
	var p2Pokemon = Array.from(new Set(p2Switches.map(mapHelper)));

	var p2Map = {};
	for (p in p2Pokemon) {
		p2Map[p2Pokemon[p]] = [];
	}
	console.log(p2Map);
	for (move in p2Moves) {
		var result = regexP2PokemonAndMove.exec(p2Moves[move]);
		if (result == null) {
			console.log("oops", p2Moves[move], result);
		} else {
			p2Map[result[1]].push(result[2]);
		}
	}

	for (p in p2Map)
		p2Map[p] = Array.from(new Set(p2Map[p]));//remove duplicates
	test = p2Map;
	console.log("map")
	console.log("map", p2Map);
	console.log(p2Pokemon);
	//console.log(p2.filter(line => regexSwitch.test(line)));
	//console.log(p2);
}

function mapHelper(str) {
	return regexP2Pokemon.exec(str)[1];

}
