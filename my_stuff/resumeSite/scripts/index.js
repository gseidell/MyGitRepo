
function init() {
    var projects = Array.from(document.getElementsByClassName("gimmepointer"));
    for (p of projects) {
        var snippet =
            `
<div id="${p.id}Modal" class="modal">
    <div class="modal-content">
      <span class="close">&times;</span>
      <img src="${p.src}" height="75%" />
      <p data-path="./assets/writeups/${p.id}.txt" id=${p.id + "modal"}></p>
</div>`
        var modal = document.createElement("DIV");
        modal.innerHTML = snippet;
        document.getElementById("Modals").appendChild(modal);

        p.onclick = show;
    }
    fillAll();
}

function fillAll() {
    elementList = Array.from(document.getElementsByTagName("*"));
    elementList.map(e => {
        if (e.getAttribute("data-path")) {
            fillWithFileContents(e.getAttribute("data-path"), e.getAttribute("id"));
        }
    });
}

function fillWithFileContents(path, id) {
    fetch(path)
        .then(r => r.text())
        .then(t => (document.getElementById(id).innerHTML = t));
}

function show(event) {
    document.getElementById(event.target.id + "Modal").style.display = "block";
}

window.onload = function () { init() }
window.onclick = function (event) {
    if (event.target.className == "modal" || event.target.className == "close") {
        Array.from(document.getElementsByClassName("modal")).map((x) => x.style.display = "none")
    }
}
window.onkeydown = function (event) {
    if (event.key == "Escape") {
        Array.from(document.getElementsByClassName("modal")).map((x) => x.style.display = "none")
    }
}
