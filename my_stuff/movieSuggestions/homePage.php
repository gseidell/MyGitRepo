<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <!-- For responsive page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Basic HTML5 Document" />
    <meta name="keywords" content="HTML5, Responsive" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">

    <title>Movies</title>
    <style>
        .main-form {
            text-align: left;
        }

        .post-img {
            width: 300px;
            height: 300px;
        }

        .post-div {
            margin: 10px;
        }

        .main-div {
            margin: 15px;
        }
    </style>
</head>

<body>

    <div class="row">
        <div class="col">
            <div class="mx-auto w-50 text-center">
                <h1>Movie Suggestions</h1>
                <hr>
                <form class="main-form" action="addSuggestion.php" method="POST" onSubmit="return onSubmit()">

                    <!-- Movie Details -->
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Movie Details: </label>
                        <div class="col-sm-6">
                            <input class="form-control" name="title" id="title" placeholder="Title" autofocus>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="year" id="year" placeholder="Year (optional)">
                        </div>
                    </div>
                    <!-- Name -->
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label">Your Name: </label>
                        <div class="col-sm-9">
				<?php
					session_start();
					$name = $_SESSION["name"];
					$line = '<input type="text" class="form-control" name="name" id="name" placeholder="Name" value="';
					$line .= $name;
					$line .= '">';
        		                echo $line;
                        	?>
						</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary">Post</button>
                        </div>
			<div class="col-sm-4">
				<input class="form-check-input" type="checkbox" id="api" name="api" checked>
				<label class="form-check-label" for="api">Pull Data from TMDB</label>
			</div>
                    </div>
                    <input type="hidden" id="poster" name="poster">
                    <input type="hidden" id="overview" name="overview">
                </form>
                <script>
                    var xhr = new XMLHttpRequest();

                    function onSubmit() {
			if(!document.getElementById("api").checked)
                        	return true;
			var request =
                            "https://api.themoviedb.org/3/search/movie?api_key=39da636eab3951b9d1cd4e1f623dbdc4&language=en-US&query=";
                        request += encodeURI(document.getElementById("title").value);
                        xhr.open('GET', request, false);
                        xhr.addEventListener("readystatechange", processRequest, false);

                        xhr.send();
                        return true;
                    }

                    function processRequest(e) {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            var response = JSON.parse(xhr.responseText);
                            document.getElementById("poster").value = response.results[0].poster_path;
                            document.getElementById("title").value = response.results[0].title;
                            document.getElementById("overview").value = response.results[0].overview;
							document.getElementById("year").value = response.results[0].release_date.substring(0,4);
                            //alert(response.results[0].poster_path);
                        }
                    }
                </script>
                <hr>
            </div>
        </div>
    </div>

    <div class="main-div">
        <!-- php code that displays all posts from database -->
        <?php
                require_once("database.php");
                require_once("genPost.php");
                $all = dbQuery("SELECT * FROM movies order by id desc");
                $n = mysqli_num_rows($all);
                $posts = "";
                foreach ($all as $key => $value) {
                    $posts .= genPost($value["id"]);
                    $posts .= "<hr width=75%>";
                }
                // for ($i = $n; $i >= 1; $i--) {
                //     $posts .= genPost($i);
                // }

                echo $posts;
            ?>

        <footer>
            <img src="tmdbLogo.png"
                alt="tmdb" height="80" width="200">
        </footer>
    </div>

</body>

</html>

