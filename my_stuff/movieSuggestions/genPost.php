<?php
    function genPost($id)
    {
        require_once("getPost.php");
        $post = getPostById($id);

        // echo print_r($post);
        $imageHTML = '<img src="http://image.tmdb.org/t/p/w185'.$post["poster"].'" width="200" height="300"/>';
        $id = $post['id'];
        $name = strip_tags($post['name']);
        $title = strip_tags($post['title']);
        $year = strip_tags($post['year']);
        $overview = strip_tags($post['overview']);
        if($name != ""){
            $name = "Suggested by ".$name;
        }
        if($year != ""){
            $year = "({$year})";
        }
        $postHTML =<<<EOPOST
        <div class="row post-div">
            <div class="col-2">
            </div>
            <div class="col-2">
                <p> $imageHTML </p>

            </div>
            <div class="col-6">
                <p> <b>$title</b> <b>$year</b></p>
                <p>$name</p>
                <p> $overview </p>

            </div>
                        <div class="col-1">
                                <form action="removeSuggestion.php" method="POST">
                                        <input type="hidden" id="postID" name="postID" value="$id">
                                        <button type="submit" class="btn btn-primary">Remove</button>
                                </form>
                        </div>

        </div>
EOPOST;

        return $postHTML;
    }


