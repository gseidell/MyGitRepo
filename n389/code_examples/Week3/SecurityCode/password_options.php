<?php
	declare(strict_types=1);
	
	$options_array = ['cost'=> 11];
	$hashed = password_hash("fear the terps", PASSWORD_DEFAULT, $options_array);
	echo "First: $hashed<br>";
	$length = strlen($hashed);
	echo "Length: $length<br>";
?>	