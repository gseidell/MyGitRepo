<?php
	declare(strict_types=1);
	$my_password = "fear the terps";
	
	$hashed = password_hash($my_password, PASSWORD_DEFAULT);
	echo "First: $hashed<br>";
	$length = strlen($hashed);
	echo "Length: $length<br>";
	
	if (password_verify($my_password, $hashed)) {
		echo "Password <strong>$my_password</strong> verified.<br>";
	} else {
		echo "Password <strong>$my_password</strong> verification failed.<br>";
	}

	$my_password .= "BAD";
	if (password_verify($my_password, $hashed)) {
		echo "Password <strong>$my_password</strong> verified.<br>";
	} else {
		echo "Password <strong>$my_password</strong> verification failed.<br>";
	}
?>	