<?php
	require_once("support.php");

	$topPart = <<<EOBODY
		<form action="{$_SERVER['PHP_SELF']}" method="post">
			<strong>Name: </strong><input type="text" name="name" /><br><br>
			<strong>Password: </strong><input type="password" name="password"/><br>
			
			<!--We need the submit button-->
			<input type="submit" name="submitInfoButton" /><br>
		</form>		
EOBODY;

	$bottomPart = "";	
	if (isset($_POST["submitInfoButton"])) {
		$nameValue = trim($_POST["name"]);
		$passwordValue = trim($_POST["password"]);
		
		if (($nameValue !== "testudo") || ($passwordValue !== "terps"))
			$bottomPart .= "<strong>Name Value Missing</strong><br />";
	}
	
	$body = $topPart.$bottomPart;	
	$page = generatePage($body);
	echo $page;
?>